#include <stdlib.h> 
#include <sys/time.h>
#include <stdio.h>
#include <math.h>
#include <omp.h>
#include <stdint.h>
#include <cmath>
#include <ctime>

double calcPi(int steps, int THREADS)
{
    double pi = 0.0;
    double factor = 1.0;

    for(int i = 0; i < steps; i++, factor =- factor) {
      pi += factor/(2*i+1);
    }

    return pi*4.0;
}

double calcPiP1(int steps, int THREADS)
{
    double pi = 0.0;
    double factor = 1.0;

#pragma omp parallel for default(none) private(factor), shared(steps) \
 reduction(+: pi) num_threads(THREADS)
    for(int i = 0; i < steps; i++) {
      if(i %2 == 0) factor = (1); else factor = (-1);
      pi += factor/(2*i+1);
    }

    return pi*4.0;
}

double calcMonteCarloPiP(int samples, int THREADS) 
{
  double pi = 0.0;  

  int counter = 0;
#pragma omp parallel shared(samples), reduction(+:counter) num_threads(THREADS)
{
  srand(time(NULL));  
  for(int i = 0; i < (samples/omp_get_num_threads()); i++) {
    double x = (double)random()/RAND_MAX;
    double y = (double)random()/RAND_MAX;
    
    if(sqrt((y * y) + (x * x)) < 1) {
      counter++;
    }
  }
}
  pi = 4 * ((double) counter / (double) samples);
  return pi;
}

int main(int argc, char** argv)
{
    int num_steps = 0, THREADS = 0, samples = 0;
    if(argc > 1) {
      num_steps = atoi(argv[1]);
      THREADS = atoi(argv[2]);
      samples = atoi(argv[3]);
    }
    else {
      fprintf(stderr, "usage: <exe> num_steps num_threads, num_samples\n");
      exit(-1);
    }

    struct timeval starttime, endtime; 

    gettimeofday(&starttime, NULL);
    double Pi0 = calcPi(num_steps, THREADS);
    gettimeofday(&endtime, NULL);
    double runtime = endtime.tv_sec + (endtime.tv_usec / 1000000.0) - starttime.tv_sec - (starttime.tv_usec / 1000000.0);
    printf("Time to calculate Pi serially with %d steps is: %g\n", num_steps, runtime);
    printf("Pi is %1.10g\n", Pi0);
    
    gettimeofday(&starttime, NULL); 
    double Pi1 = calcPiP1(num_steps, THREADS);
    gettimeofday(&endtime, NULL);
    runtime = endtime.tv_sec + (endtime.tv_usec / 1000000.0) - starttime.tv_sec - (starttime.tv_usec / 1000000.0);
    printf("Time to calculate Parallelized Pi with %d steps is: %g\n", num_steps, runtime);
    printf("Pi is %1.10g\n", Pi1);

    gettimeofday(&starttime, NULL);
    double Pi2 = calcMonteCarloPiP(samples, THREADS);
    gettimeofday(&endtime, NULL);
    runtime = endtime.tv_sec + (endtime.tv_usec / 1000000.0) - starttime.tv_sec - (starttime.tv_usec / 1000000.0);
    printf("Time to calculate Parallel Monte Carlo Pi with %d samples is: %g\n", samples, runtime);
    printf("Pi is %1.10g\n", Pi2);

    return 0;
}

