Some changes that I made to the program:
- Got rid of common.c and common.h and used a simpler timing function instead
- Added the Monte Carlo function to pi.c
- Functions used in Monte Carlo function needed g++, so I modified the Makefile slightly

Can still be compiled and ran with just:
make
./pi

The result shows the timing info and Pi calculation for the serial version, OpenMP version, and
Monte Carlo OpenMP versions.

It seems the best performing and most accurate version is the OpenMP version. The Monte Carlo version is 
nondeterministic because the more times you run the program, the resulting Pi calculation changes slightly
and the timing also is more variable. This is perhaps a result of me not having a perfect implementation. Not
sure how I can improve it.. I'll have to play with it more.

All parallel functions set to use 4 or 6 threads. The parameters I used are documented in myRun.txt. 
I only ran this on a small cluster that my research group uses. Varying
the number of threads could impact performance more if ran on Talapas. That is also something I'd need to play around
with more.
