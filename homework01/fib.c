#include <stdio.h>
#include <math.h>
#include <sys/time.h>
#include <stdlib.h>
#include <omp.h>

// Calculate Fib recursively
long unsigned int calculateFibonacci(int n)
{
    if(n == 0)
      return 0;
    else if (n == 1 || n == 2)
      return 1;
    else
      return (calculateFibonacci(n-1) + calculateFibonacci(n-2));
}

long unsigned int calculateFibonacciP(int n)
{
    long unsigned int a, b;
    if(n == 0)
      return 0;
    else if (n == 1 || n == 2)
      return 1;
    else {
#pragma omp parallel
      {
#pragma omp task shared(a)
        a = calculateFibonacciP(n-1);
#pragma omp task shared(b)
        b = calculateFibonacciP(n-2);
      }
      return a + b;
    }
}

void usage(int argc, char** argv)
{
    fprintf(stderr, "usage: %s <n>\n", argv[0]);
    exit(-1);
}

int main(int argc, char** argv)
{
    int n;
    struct timeval starttime, endtime;
    if(argc > 1) {
        n = atoi(argv[1]);
    } else {
        usage(argc, argv);
    }

    // Run & measure the naive recursive Fib series
    gettimeofday(&starttime, NULL);
    long unsigned int fib = calculateFibonacci(n);
    gettimeofday(&endtime, NULL);
    printf("\n------------------------------------------\n");
    printf("Fibonacci series for %d is %lu\n", n, fib);
    double runtime = endtime.tv_sec + (endtime.tv_usec / 1000000.0) - starttime.tv_sec - (starttime.tv_usec / 1000000.0);
    printf("\nFibonacci runtime: %.8f s\n", runtime);

    // Run & measure the parallelrecursive Fib series
    gettimeofday(&starttime, NULL);
    fib = calculateFibonacciP(n);
    gettimeofday(&endtime, NULL);
    printf("Parallel Fibonacci series for %d is %lu\n", n, fib);
    runtime = endtime.tv_sec + (endtime.tv_usec / 1000000.0) - starttime.tv_sec - (starttime.tv_usec / 1000000.0);
    printf("\nParallel Fibonacci runtime: %.8f s\n", runtime);
    return 0;
}
