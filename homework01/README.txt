I did not like how the timing code printed out the recorded timings, so I replaced it with some other code. The times are recorded in seconds.

-------

I successfully created serial and parallel Fibonacci calculation functions. I then ran 3 different experiments using the same 10 workloads (see myRun.txt). The serial implementation remains the same in each experiment, however, the parallel implementation varies slightly. Below I explain how each parallel implementation is different.

Exp1.txt:
- uses omp tasks and a omp parallel region
- parallel region has implicit barrier
- Really bad performance
- This is the version used to turn in

Exp2.txt:
- just uses omp tasks and a barrier, no omp parallel region
- Decent performance but still worse than the serial version

Exp3.txt:
- Uses the parallel region in combination with omp tasks and omp single pragma
- Performance is somewhere in between the 1st and 2nd experiments


Conclusion:
- The serial implementation is the best performing. There are some dynamic programming algorithms which can help with even better serial implementations. However, in terms of parallel implementations, I don't think the Fibonacci Sequence should be used.
