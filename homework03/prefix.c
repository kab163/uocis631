#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include <time.h>
#include <sys/time.h>
#include <string.h>
#include <math.h>

#define THREADS 4

void usage(int argc, char** argv)
{
    fprintf(stderr, "usage: %s <num elements>\n", argv[0]);
    exit(-1);
}

void verify(int* sol, int* ans, int n)
{
    int err = 0;
    for(int i = 0; i < n; i++) {
        if(sol[i] != ans[i]) {
            err++;
        }
    }
    if(err != 0) {
        fprintf(stderr, "There was an error: %d\n", err);
    } else {
        fprintf(stdout, "Pass\n");
    }
}


void prefix_sum(int* src, int* prefix, int n)
{
    for(int i = 0; i < n; i++) {
        prefix[i] = 0;
        for(int j = 0; j <= i; j++) {
            prefix[i] += src[j];
        }
    }
}

void prefix_sum0(int* src, int* prefix, int n)
{
  int sum = 0;
  for(int i = 0; i < n; i++)
  {
    sum += src[i];
    prefix[i] = sum;
  }
}


void prefix_sum1(int* src, int* prefix, int n)
{
  //to make sure we get clean numbers
  if(n % THREADS != 0) {
    fprintf(stderr, "need a perfect factor of the number of threads");
    return;
  }

  float sumTotal[THREADS+1]; //shared in this context
#pragma omp parallel num_threads(THREADS)
{
  const int myID = omp_get_thread_num();
#pragma omp single
  sumTotal[0] = 0;
        
  float sumPartial = 0; //private in this context
#pragma omp for schedule(static) nowait //using nowait actually seems to help here
  for (int i = 0; i < n; i++) {
    sumPartial += src[i];
    prefix[i] = sumPartial;
  }

  sumTotal[myID+1] = sumPartial;
#pragma omp barrier
  float offset = 0;
  for(int i = 0; i < (myID+1); i++) {
    offset += sumTotal[i];
  }

#pragma omp for schedule(static)
  for (int i = 0; i < n; i++) {
    prefix[i] += offset;
  }
}
}

void prefix_sum2(int* src, int* prefix, int n)
{
  int p1_two = 0, p1 = 0;
  int logn = log2(n);
  for(int i = 0; i <= logn; i++) {
    p1_two = 1 << (i+1);
    p1 = 1 << i;

    for(int j = 0; j < n; j+=p1_two)
      prefix[j + p1_two - 1] = src[j + p1 - 1] + src[j + p1_two - 1];
  }

  prefix[n-1] = 0;
  for(int i = logn; i >= 0; i--) {
    p1_two = 1 << (i+1);
    p1 = 1 << i;

    for(int j = 0; j < n; j+=p1_two) {
      int temp = prefix[j + p1 - 1];
      prefix[j + p1 - 1] = prefix[j + p1_two - 1];
      prefix[j + p1_two - 1] = temp + prefix[j + p1_two - 1];
    }
  }
}

int main(int argc, char** argv)
{
    int n;
    if(argc > 1) {
        n = atoi(argv[1]); 
    } else {
        usage(argc, argv);
    }

    struct timeval starttime, endtime; 
    double runtime = 0.0;
    
    int* prefix_array = (int*)malloc(sizeof(int) * n);  
    int* input_array = (int*)malloc(sizeof(int) * n);  
    srand(time(NULL));
    for(int i = 0; i < n; i++) {
        input_array[i] = rand() % 10;
    }

    // Execute this only when N is low - otherwise, it'll take forever to run
    if(n <= 4096) {
        gettimeofday(&starttime, NULL);
        prefix_sum(input_array, prefix_array, n);
        gettimeofday(&endtime, NULL);
        runtime = endtime.tv_sec + (endtime.tv_usec / 1000000.0) - starttime.tv_sec - (starttime.tv_usec / 1000000.0);
        printf("Time to do O(N^2) prefix sum on a %d element array: %.8f (s)\n", 
               n, runtime);
    }

    gettimeofday(&starttime, NULL);
    prefix_sum0(input_array, prefix_array, n);
    gettimeofday(&endtime, NULL);
    runtime = endtime.tv_sec + (endtime.tv_usec / 1000000.0) - starttime.tv_sec - (starttime.tv_usec / 1000000.0);
    printf("Time to do O(N-1) prefix sum on a %d element array: %.8f (s)\n", 
           n, runtime);

    int* input_array1 = (int*)malloc(sizeof(int) * n);  
    int* prefix_array1 = (int*)malloc(sizeof(int) * n);  
    memcpy(input_array1, input_array, sizeof(int) * n); 
    gettimeofday(&starttime, NULL);
    prefix_sum1(input_array1, prefix_array1, n);
    gettimeofday(&endtime, NULL);
    runtime = endtime.tv_sec + (endtime.tv_usec / 1000000.0) - starttime.tv_sec - (starttime.tv_usec / 1000000.0);
    printf("Time to do O(NlogN) // prefix sum on a %d element array: %.8f (s)\n", 
           n, runtime);
    verify(prefix_array, prefix_array1, n);

    memcpy(input_array1, input_array, sizeof(int) * n);
    memset(prefix_array1, 0, sizeof(int) * n);
    gettimeofday(&starttime, NULL);
    prefix_sum1(input_array1, prefix_array1, n);
    gettimeofday(&endtime, NULL);
    runtime = endtime.tv_sec + (endtime.tv_usec / 1000000.0) - starttime.tv_sec - (starttime.tv_usec / 1000000.0);
    printf("Time to do 2(N-1) // prefix sum on a %d element array: %.8f (s)\n", 
           n, runtime);
    verify(prefix_array, prefix_array1, n);

    free(prefix_array);
    free(input_array);
    free(input_array1);
    free(prefix_array1);

    return 0;
}
