-----------------------------
Notes about program
-----------------------------

To compile and run program:
make
./prefix 4096

OR (another example)

make
./prefix 1024

--

Notes:
-Make sure N provided is power of 2, otherwise program will exit with error message.
-Times measured in seconds
-Program will report runtime of faster serial version, and two parallel implementations.

--

Prefix sum seems like an interesting problem with several clever tricks to
make the performance faster. However, in terms of difficulty and user effort,
it may just be worth it to do the serial version that was O(n) - I think it
is called Sum0. If there happens to be a very large problem size (well over 1M),
then it may be worth the time and effort to figure out a performant parallel
version.
