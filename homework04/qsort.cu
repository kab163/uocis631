#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <assert.h>
#include <sys/time.h>
#include <cstdio>


#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <thrust/sort.h>
#include <thrust/copy.h>

void init_list(int* arr, int n)
{
    assert(arr);
    srand(time(NULL));
    for(int i = 0; i < n; i++) {
        arr[i] = rand() % n; 
    }
}

int cmpfunc (const void * a, const void * b) {
   return ( *(int*)a - *(int*)b );
}

void validate(int* solution, int* answer, int n)
{
    int cnt = 0;
    for(int i = 0; i < n ; i++) {
        if(solution[i] != answer[i]) {
            cnt++;
        }
    }
    fprintf(stdout, "Found %d differences\n", cnt);
}

__global__ void qsort(int *data, int left, int right, int depth)
{
  int *d_left = data+left;
  int *d_right = data+right;
  int  pivot = data[(left+right)/2]; //start with this for now...

  int leftVal, rightVal; //keeping track

  while (d_left <= d_right)
  {
    leftVal = *d_left;
    rightVal = *d_right;

    // smaller than the pivot....
    while (leftVal < pivot) {
      d_left++;
      leftVal = *d_left;
    }

    // larger than the pivot.....
    while (rightVal > pivot) {
      d_right--;
      rightVal = *d_right;
    }

    // update
    if (d_left <= d_right) {
      *d_left++ = rightVal;
      *d_right-- = leftVal;
    }
  }

  // recursion
  int num_right = d_right - data;
  int num_left = d_left - data;

  if (left < (d_right-data))  qsort<<<1,1>>>(data, left, num_right, depth+1);
  if ((d_left-data) < right)  qsort<<<1,1>>>(data, num_left, right, depth+1);
}

void qsort_gpu(int* arrAns, int* h_arr, int n)
{
  int * d_arr = 0;
  cudaMalloc((void **)&d_arr, n * sizeof(int));
  cudaMemcpy(d_arr, h_arr, n * sizeof(int), cudaMemcpyHostToDevice);

  //run kernel
  int left = 0;
  int right = n-1;
  qsort<<< 1, 1 >>>(d_arr, left, right, 0); //use one thread for now...

  cudaMemcpy(h_arr, d_arr, n * sizeof(int), cudaMemcpyDeviceToHost);

  cudaFree(d_arr);
}

int main(int argc, char** argv)
{
    // check input
    if(argc < 2) {
        fprintf(stderr, "usage: %s <num elements>\n", argv[0]);
        exit(-1);
    } 
    int n = atoi(argv[1]);
    fprintf(stdout, "Sorting %d values\n", n);

    // initialize timer
    struct timeval starttime, endtime; 
    double runtime = 0.0;

    // create input array to sort
    int* arr_Ans = (int*)malloc(sizeof(int) * n);
    assert(arr_Ans);
    init_list(arr_Ans, n);
    int* h_arr = (int*)malloc(sizeof(int) * n);
    assert(h_arr);
    memcpy(h_arr, arr_Ans, sizeof(int) * n);
    int* arr_thr = (int*)malloc(sizeof(int) * n);
    assert(arr_thr);
    memcpy(arr_thr, arr_Ans, sizeof(int) * n);

    // -----------------------------------------------------------------  
    // Sort the array using stdlib quicksort to create answer 
    gettimeofday(&starttime, NULL);
    qsort(arr_Ans, n, sizeof(int), cmpfunc);
    gettimeofday(&endtime, NULL);
    runtime = endtime.tv_sec + (endtime.tv_usec / 1000000.0) - starttime.tv_sec - (starttime.tv_usec / 1000000.0);
    printf("CPU stdlib qsort time: %.8f\n", runtime);
    // -----------------------------------------------------------------  

    // -----------------------------------------------------------------  
    // Use Nvidia's Thrust library to sort - likely implements radix
    thrust::host_vector<int> h_vec(arr_Ans, arr_Ans + n);
    thrust::device_vector<int> d_vec(arr_Ans, arr_Ans + n);
    
    gettimeofday(&starttime, NULL);
    thrust::sort(h_vec.begin(), h_vec.end());
    gettimeofday(&endtime, NULL);
    runtime = endtime.tv_sec + (endtime.tv_usec / 1000000.0) - starttime.tv_sec - (starttime.tv_usec / 1000000.0);
    printf("GPU Thrust qsort time: %.8f\n", runtime);

    std::copy(h_vec.begin(), h_vec.end(), arr_thr);

    // validate answer
    validate(arr_Ans, arr_thr, n);
    // -----------------------------------------------------------------  
    
    // -----------------------------------------------------------------  
    // Do the quicksort on GPU (assuming integer sort)
    gettimeofday(&starttime, NULL);
    qsort_gpu(arr_Ans, h_arr, n);
    gettimeofday(&endtime, NULL);
    runtime = endtime.tv_sec + (endtime.tv_usec / 1000000.0) - starttime.tv_sec - (starttime.tv_usec / 1000000.0);
    printf("GPU stdlib qsort time: %.8f\n", runtime);

    // validate answer
    validate(arr_Ans, h_arr, n);
    // -----------------------------------------------------------------  

    // cleanup
    free(arr_Ans);
    free(h_arr);
    free(arr_thr);

    return 0;
}
