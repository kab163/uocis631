To run program:

make
./qsort 1000

where 1000 is the size (n) of the array to sort.

Files in folder:
-exp1.txt contains the experiments I ran.
-exp1_results.txt contains the output of the experiments.
-Makefile is the makefile I use. This is catered to Alaska. For talapas, the nvcc location will probably have to be updated.
-qsort.cu is my source code.

From what I have seen in the experiments I ran, the CPU seems to be best able to handle the pointer chasing that
happens in the Quicksort algorithm.

Algorithm for GPU quicksort was inspired from a couple places:
https://medium.com/cesars-tech-insights/quicksort-17c5d24e7e5f
https://blogs.nvidia.com/blog/2012/09/12/how-tesla-k20-speeds-up-quicksort-a-familiar-comp-sci-code/
https://www.geeksforgeeks.org/quick-sort/

I tried to combine to get the best of both worlds and use the parts of the algorithm I liked most, but I think
more works needs to be done to get optimal GPU performance with quicksort.
